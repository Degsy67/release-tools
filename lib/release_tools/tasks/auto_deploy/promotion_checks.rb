# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module AutoDeploy
      # Perform production checks before the gstg/gprd deployments. Post a Slack
      # message and raise an error if production checks fail.
      class PromotionChecks
        include ::SemanticLogger::Loggable

        UnsafeProductionError = Class.new(StandardError)

        def initialize
          @deploy_version = ENV.fetch('DEPLOY_VERSION')
          @pipeline_url = ENV.fetch('CI_PIPELINE_URL')

          # Due to a product bug (https://gitlab.com/gitlab-org/gitlab/-/issues/288064),
          # we can't name this variable `IGNORE_PRODUCTION_CHECKS`, since it would
          # clash with the variable that is written to the dotenv artifact.
          @override_reason = ENV.fetch('OVERRIDE_PRODUCTION_CHECKS_REASON', nil)
        end

        def execute
          return if production_status.fine?

          send_slack_message

          logger.warn('Production checks have failed', failed_checks: production_status.failed_checks.collect(&:to_s), override_reason: override_reason)

          if override_reason
            add_ignore_checks_to_deploy_vars
            return
          end

          raise UnsafeProductionError, 'production checks have failed'
        end

        private

        attr_reader :deploy_version, :pipeline_url, :override_reason

        def production_status
          @production_status ||= ReleaseTools::Promotion::ProductionStatus.new(:canary_up, :active_gprd_deployments)
        end

        def send_slack_message
          Retriable.retriable do
            ReleaseTools::Slack::ChatopsNotification.fire_hook(
              text: 'gprd/gstg deployments blocked',
              channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
              blocks: foreword_slack_blocks + production_status.to_slack_blocks
            )
          end
        end

        def foreword_slack_blocks
          text = StringIO.new

          if override_reason
            text.puts(":warning: #{release_managers_mention} gprd/gstg deployments started, ignoring production checks")
            text.puts("The checks were ignored for the following reason: #{override_reason}")
          else
            text.puts(":red_circle: #{release_managers_mention} gprd/gstg deployments blocked")
          end

          text.puts
          text.puts("Package version: `#{deploy_version}`")
          text.puts("Deployment: <#{pipeline_url}|pipeline>")

          blocks = ::Slack::BlockKit.blocks
          blocks.section { |block| block.mrkdwn(text: text.string) }
          blocks.as_json
        end

        def release_managers_mention
          "<!subteam^#{ReleaseTools::Slack::RELEASE_MANAGERS}>"
        end

        def add_ignore_checks_to_deploy_vars
          File.open('deploy_vars.env', 'a') do |deploy_vars|
            deploy_vars << "IGNORE_PRODUCTION_CHECKS=\"#{override_reason}\""
          end
        end
      end
    end
  end
end
