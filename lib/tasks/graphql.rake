# frozen_string_literal: true

namespace :graphql do
  desc 'Update the GraphQL schema dump'
  task :dump_schema do
    ReleaseTools::GitlabClient.graphql_client.dump_schema
  end
end
