#!/usr/bin/env bash

# This script will install the ASDF plugins required for this project
# Inspired by https://gitlab.com/gitlab-com/runbooks/-/blob/master/scripts/install-asdf-plugins.sh

set -euo pipefail
IFS=$'\n\t'

# shellcheck source=/dev/null
source "$ASDF_DIR/asdf.sh"

plugin_list=$(asdf plugin list || echo)

install_plugin() {
  local plugin=$1

  if ! echo "${plugin_list}" | grep -q "${plugin}"; then
    echo "# Installing plugin" "$@"
    asdf plugin add "$@" || {
      echo "Failed to install plugin:" "$@"
      exit 1
    } >&2
  fi

  echo "# Installing ${plugin} version"
  asdf install "${plugin}" || {
    echo "Failed to install plugin version: ${plugin}"
    exit 1
  } >&2

  # Use this plugin for the rest of the install-asdf-plugins.sh script...
  asdf shell "${plugin}" "$(asdf current "${plugin}" | awk '{print $2}')"
}

# Install golang first as some of the other plugins require it.
install_plugin golang

install_plugin golangci-lint
install_plugin mockery
install_plugin ruby
