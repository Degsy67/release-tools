package metrics

type Label interface {
	Name() string
	CheckValue(value string) bool
	Values() []string
}
